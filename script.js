var semester = document.getElementById("semester"); /* Semester input */
semester.addEventListener("input", calculate); /* Check for changes */

var eap = document.getElementById("eap"); /* EAP input */
eap.addEventListener("input", calculate); /* Check for changes */

var loadElement = document.getElementById("load"); /* Load type span */
var fineElement = document.getElementById("fine"); /* Fine-or-no-fine span */

var fullFreeEap = 30; /* Full EAP by regulation */
var eapBuffer = 6; /* Buffer EAP for free load without fines */
var minFreeEap = 22.5; /* Minimum EAP for free load with fines / Full EAP for paid load (when rounded) */
var minPaidEap = 15; /* Minimum EAP for paid load */
var finePerEap = 30; /* Fine amount in euros per EAP */

var str_free = "jään tasuta täiskoormusesse";
var str_paid = "langen tasulisse osakoormusesse";
var str_drop = "saan eksmatrikuleeritud";
var str_full = "täidan õppekava täies mahus";
var str_no_fine = "ei pea trahvi maksma";
var str_fine = "pean maksma trahvi ";

calculate(); /* Initial calculation, to check for JS availability */

function calculate() {
    var fullEapForSemester = (fullFreeEap * semester.value); /* Required EAP for a full free X semester */
    var freeEapForSemester = fullEapForSemester - eapBuffer; /* Required EAP for a free X semester (with buffer) */
    var paidEapForSemester = (minFreeEap * semester.value); /* Required EAP for a full paid (minimum free) X semester */
    var minPaidEapForSemester = (minPaidEap * semester.value); /* Minimum required EAP for a paid X semester */

    var fine = (freeEapForSemester - eap.value) * finePerEap; /* Fine sum */
    var extra = eap.value - fullEapForSemester; /* Extra EAP */

    if (eap.value > fullEapForSemester) { /* Full free semester with extra */
        loadElement.innerHTML = str_free;
        fineElement.innerHTML = str_full + " (+" + extra + " EAP)";
    }
    else if (eap.value == fullEapForSemester) { /* Full free semester */
        loadElement.innerHTML = str_free;
        fineElement.innerHTML = str_full;
    }
    else if (eap.value >= freeEapForSemester && eap.value < fullEapForSemester) { /* Free semester */
        loadElement.innerHTML = str_free;
        fineElement.innerHTML = str_no_fine;
    }
    else if (eap.value >= paidEapForSemester && eap.value < freeEapForSemester) { /* Free semester with a fine */
        loadElement.innerHTML = str_free;
        fineElement.innerHTML = str_fine + fine + "€";
    }
    else if (eap.value >= minPaidEapForSemester && eap.value < paidEapForSemester) { /* Paid semester */
        loadElement.innerHTML = str_paid;
        fineElement.innerHTML = str_fine + fine + "€"; /* Assumption that the user only falls to paid, instead of voluntarily choosing it. */
    }
    else if (eap.value < minPaidEapForSemester) { /* Dropout */
        loadElement.innerHTML = str_drop;
        fineElement.innerHTML = str_fine + fine + "€";
    }
}